# Training 

- This repo includes work done as a part of training for backend development

- This also includes some of the tasks that were assigned and have now been deployed on production

## Django-ES
###Features:
- An implementation of ES integration with django
- Auto populates data using django commands
- Auto indexes students data into ES
- Search students using names

### Instructions

To Populate dummy data ( make sure ES is up. Verify by `curl -XGET http://localhost:9200`:

`python manage.py dummy_data`

To search a student:

- Open python shell do the following:

	- `from django_es.search import *`

	- `print(search('Jorge'))`
	
### Scope:
- ngram implementation for partial text matching
- course and university indexing (not implemented currently due to complex mappings)
