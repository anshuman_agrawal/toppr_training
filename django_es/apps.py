from django.apps import AppConfig


class django_es_config(AppConfig):
    name = 'django_es'

    def ready(self):
        import django_es.signals