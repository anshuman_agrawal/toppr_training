# from model_mommy import mommy
import random
import names
from django.core.management.base import BaseCommand
from django_es.models import University, Course, Student
from django.db import transaction, DatabaseError
from django.db import connection
class Command(BaseCommand):
    help = "My shiny new management command."

    def add_arguments(self, parser):
        parser.add_argument('count', nargs=1, type=int)

    def handle(self, *args, **options):
        print 'running command'

        self.make_universities()
        self.make_courses()
        self.make_students(options)
    def make_universities(self):
        university_names = (
            'MIT', 'MGU', 'CalTech', 'KPI', 'DPI', 'PSTU'
        )
        self.universities = []
        for name in university_names:
            try:
                uni = University.objects.create(name=name)
            except Exception as e:
                print
                # e.message, e.args
                # print('error creating %s', name)
                connection._rollback()


    def make_courses(self):
        template_options = ['CS%s0%s', 'MATH%s0%s', 'CHEM%s0%s', 'PHYS%s0%s']
        for num in range(1, 4):
            for course_num in range(1, 4):
                for template in template_options:
                    name = template % (course_num, num)
                    try:
                        course = Course.objects.create(name=name)
                    except Exception as e:
                        # print e.message, e.args
                        # print('error creating %s',name)
                        connection._rollback()

    def make_students(self,options):
        self.students = []
        for _ in xrange(1000):
            stud = Student.objects.create(university=random.choice(University.objects.all()),
                           year_in_school= random.choice(['FR','SO','SR','JR']),
                first_name=names.get_first_name(),
                last_name=names.get_last_name(),
                age=random.randint(17, 25))
            courses = list(Course.objects.all())
            enrolled = random.randint(1,5)
            for i in range(enrolled):
                course_taken = random.choice(courses)
                stud.courses.add(course_taken)
                courses.remove(course_taken)
            stud.save()

        # Student.objects.bulk_create(self.students)
