# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'University'
        db.create_table('django_es_university', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
        ))
        db.send_create_signal('django_es', ['University'])

        # Adding model 'Course'
        db.create_table('django_es_course', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
        ))
        db.send_create_signal('django_es', ['Course'])

        # Adding model 'Student'
        db.create_table('django_es_student', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('year_in_school', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('age', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('university', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['django_es.University'], null=True, blank=True)),
        ))
        db.send_create_signal('django_es', ['Student'])

        # Adding M2M table for field courses on 'Student'
        m2m_table_name = db.shorten_name('django_es_student_courses')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('student', models.ForeignKey(orm['django_es.student'], null=False)),
            ('course', models.ForeignKey(orm['django_es.course'], null=False))
        ))
        db.create_unique(m2m_table_name, ['student_id', 'course_id'])


    def backwards(self, orm):
        # Deleting model 'University'
        db.delete_table('django_es_university')

        # Deleting model 'Course'
        db.delete_table('django_es_course')

        # Deleting model 'Student'
        db.delete_table('django_es_student')

        # Removing M2M table for field courses on 'Student'
        db.delete_table(db.shorten_name('django_es_student_courses'))


    models = {
        'django_es.course': {
            'Meta': {'object_name': 'Course'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        'django_es.student': {
            'Meta': {'object_name': 'Student'},
            'age': ('django.db.models.fields.SmallIntegerField', [], {}),
            'courses': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['django_es.Course']", 'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'university': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['django_es.University']", 'null': 'True', 'blank': 'True'}),
            'year_in_school': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        'django_es.university': {
            'Meta': {'object_name': 'University'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        }
    }

    complete_apps = ['django_es']