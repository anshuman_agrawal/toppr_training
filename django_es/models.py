from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from .search import studentIndex
class University(models.Model):
    name = models.CharField(max_length=255, unique=True)
class Course(models.Model):
    name = models.CharField(max_length=255, unique=True)
class Student(models.Model):
    YEAR_IN_SCHOOL_CHOICES = (
        ('FR', 'Freshman'),
        ('SO', 'Sophomore'),
        ('JR', 'Junior'),
        ('SR', 'Senior'),
    )
    # note: incorrect choice in MyModel.create leads to creation of incorrect record
    year_in_school = models.CharField(
        max_length=2, choices=YEAR_IN_SCHOOL_CHOICES)
    age = models.SmallIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(100)]
    )
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    # various relationships models
    university = models.ForeignKey(University, null=True, blank=True)
    courses = models.ManyToManyField(Course, null=True, blank=True)

    def indexing(self):
        print ('indexing user',self.first_name)
        obj = studentIndex(
            meta={'id': self.id},
            year_in_school = self.year_in_school,
            age = self.age,
            first_name = self.first_name,
            last_name = self.last_name,
            # university = self.university,
            # courses = self.courses

        )
        obj.save()
        return obj.to_dict(include_meta=True)
    def save(self,*args,**kwargs):
        if not self.pk:
            self.indexing()
        super(Student,self).save(*args,**kwargs)