from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType, Text, Date, Search
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch
import models
from django.http import HttpResponse

connections.create_connection()

class courseIndex(DocType):
    name = Text()

class studentIndex(DocType):
    year_in_school = Text()
    age = Text()
    first_name = Text()
    last_name = Text()
    # university = Text()
    # courses = Text()

    class Meta:
        index = 'student-index'

def bulk_indexing():
    studentIndex.init()
    es = Elasticsearch()
    bulk(client=es, actions=(b.indexing() for b in models.Student.objects.all().iterator()))

def search(student_name):
    to_search = student_name+'*'
    s = Search().query("match", first_name=student_name)
    response = s.execute()
    return response
