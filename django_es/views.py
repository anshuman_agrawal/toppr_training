from django.http import HttpResponse
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType, Text, Date, Search
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch
import models
import json

def index(request):
    return HttpResponse("Hello, world. You're at the index.")

def search(request,name):
    to_search = name
    s = Search().query("match", first_name=to_search)
    response = s.execute()
    li=[]
    print (response)
    for hit in s:
        li.append(hit.first_name+' '+hit.last_name+'\n\n')
    return HttpResponse(li)