import logging
from core.api.mock import FileMockerMixin
from webhooks.models import GlobalSystemEventsHook
from core.api.responses.v1 import APIResponse
import json
from django.db import transaction
import datetime
from dateutil.parser import parse
from django.utils import timezone
log=logging.getLogger("www")







class SystemEventsWebHook(APIResponse, FileMockerMixin):

    __versions_compatible__ = ('1', '1.0')

    def get_or_create_data(self):
        # Handle mocking
        self.ignored_cases = []
        self._data = self.get_mock_response()
        if self._data: return self._data
        ctxt = {}
        self._data = ctxt  # Set the data to ctxt

        # References to object variables for quick access
        request = self.request

        if request.method != 'POST':
            self.set_bad_req('invalid request')
            return self._data

        request_data = json.loads(request.body)
        data = self.get_data(request_data)
        data['request_json'] = json.dumps(request_data)
        try:
            glo_sys_eve_hook = GlobalSystemEventsHook(**data)
            glo_sys_eve_hook.save()
        except Exception, e:
            self.set_error("Something went wrong")
            log.exception(e)

        return self._data

    def get_data(self,request_data):
        data = {}
        data.update({
            'event_time': request_data.get('event_time'),
            'event_name': request_data.get('event_name'),
            'anonymous_id': request_data.get('anonymous_id'),
            'event_type': request_data.get('event_name'),
            'category': request_data.get('category'),
            'interface_id': request_data.get('interface_id'),
            'license_code': request_data.get('license_code'),
            'browser': request_data.get('browser_name'),
            'browser_version_code': request_data.get('browser_version'),
            'referrer_type': request_data.get('referrer_type'),
            'user_id': request_data.get('user_id')

        })

        event_time = request_data.get('event_time')
        created_on = timezone.now()
        data['created_on'] = created_on
        try:
            data['event_time_timestamp'] = parse(event_time)
        except:
            pass

        system_data = request_data.get('system_data')
        event_data = request_data.get('event_data')
        if system_data:
            data.update({
                'locale': system_data.get('locale'),
                'variation_id': system_data.get('variation_id'),
                'journey_id': system_data.get('journey_id'),
                'scope': system_data.get('scope'),
                'screen_title': system_data.get('screen_title'),
                'screen_path': system_data.get('screen_path'),
                'city': system_data.get('city'),
                'os_version': system_data.get('os_version'),
                'sdk_version': system_data.get('sdk_version'),
                'app_version_code': system_data.get('app_version_code'),
                'os_name': system_data.get('os_name'),
                'device_type': system_data.get('device_type'),
                'device_brand': system_data.get('brand'),
                'device_model': system_data.get('model'),
                'manufacturer': system_data.get('manufacturer'),
                'sdk': system_data.get('sdk'),
                'app_version': system_data.get('app_version'),
                'language': system_data.get('language'),
                'country': system_data.get('country'),
                'region': system_data.get('region'),
                'api_version': system_data.get('api_version'),
                'campaign_id': system_data.get('campaign_id'),
                'locality': system_data.get('locality')
            })
            viewport_height = system_data.get('viewport_height')
            viewport_width = system_data.get('viewport_width')
            longitude = system_data.get('longitude')
            latitude = system_data.get('latitude')
            postal_code = system_data.get('postal_code')
            try:
                data['postal_code'] = int(postal_code)
            except:
                pass
            try:
                data['longitude'] = float(longitude)
            except:
                pass
            try:
                data['latitude'] = float(latitude)
            except:
                pass
            try:
                data['viewport_height'] = int(viewport_height)
            except:
                pass
            try:
                data['viewport_width'] = int(viewport_width)
            except:
                pass
            push_notification_content = system_data.get('push_notification_content')
        else:
            push_notification_content = None
        if push_notification_content:
            data.update({
                'identifier_id': push_notification_content.get('identifier'),
                'experiment_id': push_notification_content.get('experimentId'),
                'package_name': push_notification_content.get('packageName'),
                'title': push_notification_content.get('title'),
                'message': push_notification_content.get('message'),
                'image_url': push_notification_content.get('image'),
                'priority': push_notification_content.get('priority')
            })
            time_to_live = push_notification_content.get('timeToLive')
            try:
                data['time_to_live'] = parse(time_to_live)
            except:
                pass
            cta = push_notification_content.get('cta')
            expandable_details = push_notification_content.get('expandableDetails')
        else:
            cta = None
            expandable_details = None
        if cta:
            data.update({
                'cta_id': cta.get('id'),
                'cta_type': cta.get('type'),
                'action_link': cta.get('actionLink'),
                'action_text': cta.get('actionText'),
                'template_id': cta.get('templateId'),
            })
        if event_data:
            data.update({
                'result': event_data.get('result'),
                'gcm_project_number': event_data.get('gcm_project_number'),
                'gcm_regid': event_data.get('gcm_regId'),
                'reason': event_data.get('reason'),
                'new_time_to_live': event_data.get('new_time_to_live')

            })
            bucket_value = event_data.get('bucket_value')
            send_next = event_data.get('send_next')
            try:
                data['send_next'] = parse(send_next)
            except:
                pass
            try:
                data['bucket_value'] = int(bucket_value)
            except:
                pass
        if expandable_details:
            data['style'] = expandable_details.get('style')
            rating_scale = expandable_details.get('ratingScale')
            try:
                data['rating_scale'] = int(rating_scale)
            except:
                pass
        return data



