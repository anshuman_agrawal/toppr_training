# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'JourneyHook'
        db.create_table('webhooks_journeyhook', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('resource', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('user_id', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('creation_time', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('journey_name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('category', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('event_time', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('created_by', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('event_name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('done_by', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('done_at', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('tags', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('request_json', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal('webhooks', ['JourneyHook'])

        # Adding model 'GlobalSystemEventsHook'
        db.create_table('webhooks_globalsystemeventshook', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('event_time', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('event_name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('user_id', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('request_json', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal('webhooks', ['GlobalSystemEventsHook'])


    def backwards(self, orm):
        # Deleting model 'JourneyHook'
        db.delete_table('webhooks_journeyhook')

        # Deleting model 'GlobalSystemEventsHook'
        db.delete_table('webhooks_globalsystemeventshook')


    models = {
        'webhooks.globalsystemeventshook': {
            'Meta': {'object_name': 'GlobalSystemEventsHook'},
            'event_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'event_time': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'request_json': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'})
        },
        'webhooks.journeyhook': {
            'Meta': {'object_name': 'JourneyHook'},
            'category': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'created_by': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'creation_time': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'done_at': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'done_by': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'event_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'event_time': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'journey_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'request_json': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'resource': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'tags': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'})
        }
    }

    complete_apps = ['webhooks']