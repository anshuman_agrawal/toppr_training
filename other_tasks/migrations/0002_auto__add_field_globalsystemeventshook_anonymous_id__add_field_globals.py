# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'GlobalSystemEventsHook.event_time_timestamp'
        db.add_column('webhooks_globalsystemeventshook', 'event_time_timestamp',
                      self.gf('django.db.models.fields.DateTimeField')(default=None, null=True, blank=True),
                      keep_default=True)

        # Adding field 'GlobalSystemEventsHook.anonymous_id'
        db.add_column('webhooks_globalsystemeventshook', 'anonymous_id',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.event_type'
        db.add_column('webhooks_globalsystemeventshook', 'event_type',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.category'
        db.add_column('webhooks_globalsystemeventshook', 'category',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.locale'
        db.add_column('webhooks_globalsystemeventshook', 'locale',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.variation_id'
        db.add_column('webhooks_globalsystemeventshook', 'variation_id',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.campaign_id'
        db.add_column('webhooks_globalsystemeventshook', 'campaign_id',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.journey_id'
        db.add_column('webhooks_globalsystemeventshook', 'journey_id',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.interface_id'
        db.add_column('webhooks_globalsystemeventshook', 'interface_id',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.identifier_id'
        db.add_column('webhooks_globalsystemeventshook', 'identifier_id',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.experiment_id'
        db.add_column('webhooks_globalsystemeventshook', 'experiment_id',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.package_name'
        db.add_column('webhooks_globalsystemeventshook', 'package_name',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.title'
        db.add_column('webhooks_globalsystemeventshook', 'title',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.message'
        db.add_column('webhooks_globalsystemeventshook', 'message',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.image_url'
        db.add_column('webhooks_globalsystemeventshook', 'image_url',
                      self.gf('django.db.models.fields.TextField')(default=None, null=True, blank=True),
                      keep_default=True)

        # Adding field 'GlobalSystemEventsHook.cta_id'
        db.add_column('webhooks_globalsystemeventshook', 'cta_id',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.cta_type'
        db.add_column('webhooks_globalsystemeventshook', 'cta_type',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.action_link'
        db.add_column('webhooks_globalsystemeventshook', 'action_link',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.action_text'
        db.add_column('webhooks_globalsystemeventshook', 'action_text',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.template_id'
        db.add_column('webhooks_globalsystemeventshook', 'template_id',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.scope'
        db.add_column('webhooks_globalsystemeventshook', 'scope',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.screen_title'
        db.add_column('webhooks_globalsystemeventshook', 'screen_title',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.screen_path'
        db.add_column('webhooks_globalsystemeventshook', 'screen_path',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.reason'
        db.add_column('webhooks_globalsystemeventshook', 'reason',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.result'
        db.add_column('webhooks_globalsystemeventshook', 'result',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.new_time_to_live'
        db.add_column('webhooks_globalsystemeventshook', 'new_time_to_live',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.style'
        db.add_column('webhooks_globalsystemeventshook', 'style',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.priority'
        db.add_column('webhooks_globalsystemeventshook', 'priority',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.city'
        db.add_column('webhooks_globalsystemeventshook', 'city',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.locality'
        db.add_column('webhooks_globalsystemeventshook', 'locality',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.os_version'
        db.add_column('webhooks_globalsystemeventshook', 'os_version',
                      self.gf('django.db.models.fields.CharField')(max_length=100, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.sdk_version'
        db.add_column('webhooks_globalsystemeventshook', 'sdk_version',
                      self.gf('django.db.models.fields.CharField')(max_length=100, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.app_version_code'
        db.add_column('webhooks_globalsystemeventshook', 'app_version_code',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.os_name'
        db.add_column('webhooks_globalsystemeventshook', 'os_name',
                      self.gf('django.db.models.fields.CharField')(max_length=100, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.device_type'
        db.add_column('webhooks_globalsystemeventshook', 'device_type',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.device_brand'
        db.add_column('webhooks_globalsystemeventshook', 'device_brand',
                      self.gf('django.db.models.fields.CharField')(max_length=100, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.device_model'
        db.add_column('webhooks_globalsystemeventshook', 'device_model',
                      self.gf('django.db.models.fields.CharField')(max_length=100, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.manufacturer'
        db.add_column('webhooks_globalsystemeventshook', 'manufacturer',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.sdk'
        db.add_column('webhooks_globalsystemeventshook', 'sdk',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.app_version'
        db.add_column('webhooks_globalsystemeventshook', 'app_version',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.license_code'
        db.add_column('webhooks_globalsystemeventshook', 'license_code',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.browser'
        db.add_column('webhooks_globalsystemeventshook', 'browser',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.browser_version_code'
        db.add_column('webhooks_globalsystemeventshook', 'browser_version_code',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.referrer_type'
        db.add_column('webhooks_globalsystemeventshook', 'referrer_type',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.language'
        db.add_column('webhooks_globalsystemeventshook', 'language',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.country'
        db.add_column('webhooks_globalsystemeventshook', 'country',
                      self.gf('django.db.models.fields.CharField')(max_length=100, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.region'
        db.add_column('webhooks_globalsystemeventshook', 'region',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.api_version'
        db.add_column('webhooks_globalsystemeventshook', 'api_version',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.gcm_regid'
        db.add_column('webhooks_globalsystemeventshook', 'gcm_regid',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.gcm_project_number'
        db.add_column('webhooks_globalsystemeventshook', 'gcm_project_number',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.created_on'
        db.add_column('webhooks_globalsystemeventshook', 'created_on',
                      self.gf('django.db.models.fields.DateTimeField')(default=None, null=True, blank=True),
                      keep_default=True)

        # Adding field 'GlobalSystemEventsHook.time_to_live'
        db.add_column('webhooks_globalsystemeventshook', 'time_to_live',
                      self.gf('django.db.models.fields.DateTimeField')(default=None, null=True, blank=True),
                      keep_default=True)

        # Adding field 'GlobalSystemEventsHook.send_next'
        db.add_column('webhooks_globalsystemeventshook', 'send_next',
                      self.gf('django.db.models.fields.DateTimeField')(default=None, null=True, blank=True),
                      keep_default=True)

        # Adding field 'GlobalSystemEventsHook.bucket_value'
        db.add_column('webhooks_globalsystemeventshook', 'bucket_value',
                      self.gf('django.db.models.fields.IntegerField')(max_length=10, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.rating_scale'
        db.add_column('webhooks_globalsystemeventshook', 'rating_scale',
                      self.gf('django.db.models.fields.IntegerField')(max_length=10, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.viewport_height'
        db.add_column('webhooks_globalsystemeventshook', 'viewport_height',
                      self.gf('django.db.models.fields.IntegerField')(max_length=10, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.viewport_width'
        db.add_column('webhooks_globalsystemeventshook', 'viewport_width',
                      self.gf('django.db.models.fields.IntegerField')(max_length=10, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.postal_code'
        db.add_column('webhooks_globalsystemeventshook', 'postal_code',
                      self.gf('django.db.models.fields.IntegerField')(max_length=10, null=True),
                      keep_default=False)

        # Adding field 'GlobalSystemEventsHook.longitude'
        db.add_column('webhooks_globalsystemeventshook', 'longitude',
                      self.gf('django.db.models.fields.DecimalField')(default=None, null=True, max_digits=10,
                                                                      decimal_places=6, db_index=True),
                      keep_default=True)

        # Adding field 'GlobalSystemEventsHook.latitude'
        db.add_column('webhooks_globalsystemeventshook', 'latitude',
                      self.gf('django.db.models.fields.DecimalField')(default=None, null=True, max_digits=10,
                                                                      decimal_places=6, db_index=True),
                      keep_default=True)

        # Changing field 'GlobalSystemEventsHook.event_name'
        db.alter_column('webhooks_globalsystemeventshook', 'event_name',
                        self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'GlobalSystemEventsHook.event_time'
        db.alter_column('webhooks_globalsystemeventshook', 'event_time',
                        self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

    def backwards(self, orm):
        # Deleting field 'GlobalSystemEventsHook.event_time_timestamp'
        db.delete_column('webhooks_globalsystemeventshook', 'event_time_timestamp')

        # Deleting field 'GlobalSystemEventsHook.anonymous_id'
        db.delete_column('webhooks_globalsystemeventshook', 'anonymous_id')

        # Deleting field 'GlobalSystemEventsHook.event_type'
        db.delete_column('webhooks_globalsystemeventshook', 'event_type')

        # Deleting field 'GlobalSystemEventsHook.category'
        db.delete_column('webhooks_globalsystemeventshook', 'category')

        # Deleting field 'GlobalSystemEventsHook.locale'
        db.delete_column('webhooks_globalsystemeventshook', 'locale')

        # Deleting field 'GlobalSystemEventsHook.variation_id'
        db.delete_column('webhooks_globalsystemeventshook', 'variation_id')

        # Deleting field 'GlobalSystemEventsHook.campaign_id'
        db.delete_column('webhooks_globalsystemeventshook', 'campaign_id')

        # Deleting field 'GlobalSystemEventsHook.journey_id'
        db.delete_column('webhooks_globalsystemeventshook', 'journey_id')

        # Deleting field 'GlobalSystemEventsHook.interface_id'
        db.delete_column('webhooks_globalsystemeventshook', 'interface_id')

        # Deleting field 'GlobalSystemEventsHook.identifier_id'
        db.delete_column('webhooks_globalsystemeventshook', 'identifier_id')

        # Deleting field 'GlobalSystemEventsHook.experiment_id'
        db.delete_column('webhooks_globalsystemeventshook', 'experiment_id')

        # Deleting field 'GlobalSystemEventsHook.package_name'
        db.delete_column('webhooks_globalsystemeventshook', 'package_name')

        # Deleting field 'GlobalSystemEventsHook.title'
        db.delete_column('webhooks_globalsystemeventshook', 'title')

        # Deleting field 'GlobalSystemEventsHook.message'
        db.delete_column('webhooks_globalsystemeventshook', 'message')

        # Deleting field 'GlobalSystemEventsHook.image_url'
        db.delete_column('webhooks_globalsystemeventshook', 'image_url')

        # Deleting field 'GlobalSystemEventsHook.cta_id'
        db.delete_column('webhooks_globalsystemeventshook', 'cta_id')

        # Deleting field 'GlobalSystemEventsHook.cta_type'
        db.delete_column('webhooks_globalsystemeventshook', 'cta_type')

        # Deleting field 'GlobalSystemEventsHook.action_link'
        db.delete_column('webhooks_globalsystemeventshook', 'action_link')

        # Deleting field 'GlobalSystemEventsHook.action_text'
        db.delete_column('webhooks_globalsystemeventshook', 'action_text')

        # Deleting field 'GlobalSystemEventsHook.template_id'
        db.delete_column('webhooks_globalsystemeventshook', 'template_id')

        # Deleting field 'GlobalSystemEventsHook.scope'
        db.delete_column('webhooks_globalsystemeventshook', 'scope')

        # Deleting field 'GlobalSystemEventsHook.screen_title'
        db.delete_column('webhooks_globalsystemeventshook', 'screen_title')

        # Deleting field 'GlobalSystemEventsHook.screen_path'
        db.delete_column('webhooks_globalsystemeventshook', 'screen_path')

        # Deleting field 'GlobalSystemEventsHook.reason'
        db.delete_column('webhooks_globalsystemeventshook', 'reason')

        # Deleting field 'GlobalSystemEventsHook.result'
        db.delete_column('webhooks_globalsystemeventshook', 'result')

        # Deleting field 'GlobalSystemEventsHook.new_time_to_live'
        db.delete_column('webhooks_globalsystemeventshook', 'new_time_to_live')

        # Deleting field 'GlobalSystemEventsHook.style'
        db.delete_column('webhooks_globalsystemeventshook', 'style')

        # Deleting field 'GlobalSystemEventsHook.priority'
        db.delete_column('webhooks_globalsystemeventshook', 'priority')

        # Deleting field 'GlobalSystemEventsHook.city'
        db.delete_column('webhooks_globalsystemeventshook', 'city')

        # Deleting field 'GlobalSystemEventsHook.locality'
        db.delete_column('webhooks_globalsystemeventshook', 'locality')

        # Deleting field 'GlobalSystemEventsHook.os_version'
        db.delete_column('webhooks_globalsystemeventshook', 'os_version')

        # Deleting field 'GlobalSystemEventsHook.sdk_version'
        db.delete_column('webhooks_globalsystemeventshook', 'sdk_version')

        # Deleting field 'GlobalSystemEventsHook.app_version_code'
        db.delete_column('webhooks_globalsystemeventshook', 'app_version_code')

        # Deleting field 'GlobalSystemEventsHook.os_name'
        db.delete_column('webhooks_globalsystemeventshook', 'os_name')

        # Deleting field 'GlobalSystemEventsHook.device_type'
        db.delete_column('webhooks_globalsystemeventshook', 'device_type')

        # Deleting field 'GlobalSystemEventsHook.device_brand'
        db.delete_column('webhooks_globalsystemeventshook', 'device_brand')

        # Deleting field 'GlobalSystemEventsHook.device_model'
        db.delete_column('webhooks_globalsystemeventshook', 'device_model')

        # Deleting field 'GlobalSystemEventsHook.manufacturer'
        db.delete_column('webhooks_globalsystemeventshook', 'manufacturer')

        # Deleting field 'GlobalSystemEventsHook.sdk'
        db.delete_column('webhooks_globalsystemeventshook', 'sdk')

        # Deleting field 'GlobalSystemEventsHook.app_version'
        db.delete_column('webhooks_globalsystemeventshook', 'app_version')

        # Deleting field 'GlobalSystemEventsHook.license_code'
        db.delete_column('webhooks_globalsystemeventshook', 'license_code')

        # Deleting field 'GlobalSystemEventsHook.browser'
        db.delete_column('webhooks_globalsystemeventshook', 'browser')

        # Deleting field 'GlobalSystemEventsHook.browser_version_code'
        db.delete_column('webhooks_globalsystemeventshook', 'browser_version_code')

        # Deleting field 'GlobalSystemEventsHook.referrer_type'
        db.delete_column('webhooks_globalsystemeventshook', 'referrer_type')

        # Deleting field 'GlobalSystemEventsHook.language'
        db.delete_column('webhooks_globalsystemeventshook', 'language')

        # Deleting field 'GlobalSystemEventsHook.country'
        db.delete_column('webhooks_globalsystemeventshook', 'country')

        # Deleting field 'GlobalSystemEventsHook.region'
        db.delete_column('webhooks_globalsystemeventshook', 'region')

        # Deleting field 'GlobalSystemEventsHook.api_version'
        db.delete_column('webhooks_globalsystemeventshook', 'api_version')

        # Deleting field 'GlobalSystemEventsHook.gcm_regid'
        db.delete_column('webhooks_globalsystemeventshook', 'gcm_regid')

        # Deleting field 'GlobalSystemEventsHook.gcm_project_number'
        db.delete_column('webhooks_globalsystemeventshook', 'gcm_project_number')

        # Deleting field 'GlobalSystemEventsHook.created_on'
        db.delete_column('webhooks_globalsystemeventshook', 'created_on')

        # Deleting field 'GlobalSystemEventsHook.time_to_live'
        db.delete_column('webhooks_globalsystemeventshook', 'time_to_live')

        # Deleting field 'GlobalSystemEventsHook.send_next'
        db.delete_column('webhooks_globalsystemeventshook', 'send_next')

        # Deleting field 'GlobalSystemEventsHook.bucket_value'
        db.delete_column('webhooks_globalsystemeventshook', 'bucket_value')

        # Deleting field 'GlobalSystemEventsHook.rating_scale'
        db.delete_column('webhooks_globalsystemeventshook', 'rating_scale')

        # Deleting field 'GlobalSystemEventsHook.viewport_height'
        db.delete_column('webhooks_globalsystemeventshook', 'viewport_height')

        # Deleting field 'GlobalSystemEventsHook.viewport_width'
        db.delete_column('webhooks_globalsystemeventshook', 'viewport_width')

        # Deleting field 'GlobalSystemEventsHook.postal_code'
        db.delete_column('webhooks_globalsystemeventshook', 'postal_code')

        # Deleting field 'GlobalSystemEventsHook.longitude'
        db.delete_column('webhooks_globalsystemeventshook', 'longitude')

        # Deleting field 'GlobalSystemEventsHook.latitude'
        db.delete_column('webhooks_globalsystemeventshook', 'latitude')

        # Changing field 'GlobalSystemEventsHook.event_name'
        db.alter_column('webhooks_globalsystemeventshook', 'event_name',
                        self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'GlobalSystemEventsHook.event_time'
        db.alter_column('webhooks_globalsystemeventshook', 'event_time',
                        self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

    models = {
        'webhooks.globalsystemeventshook': {
            'Meta': {'object_name': 'GlobalSystemEventsHook'},
            'action_link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'action_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'anonymous_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'api_version': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'app_version': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'app_version_code': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'browser': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'browser_version_code': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'bucket_value': ('django.db.models.fields.IntegerField', [], {'max_length': '10', 'null': 'True'}),
            'campaign_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'category': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'created_on': (
            'django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'cta_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'cta_type': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'device_brand': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'device_model': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'device_type': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'event_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'event_time': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'event_time_timestamp': (
            'django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'event_type': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'experiment_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'gcm_project_number': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'gcm_regid': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identifier_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'image_url': (
            'django.db.models.fields.TextField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'interface_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'journey_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [],
                         {'default': 'None', 'null': 'True', 'max_digits': '10', 'decimal_places': '6',
                          'db_index': 'True'}),
            'license_code': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'locale': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'locality': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'longitude': ('django.db.models.fields.DecimalField', [],
                          {'default': 'None', 'null': 'True', 'max_digits': '10', 'decimal_places': '6',
                           'db_index': 'True'}),
            'manufacturer': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'new_time_to_live': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'os_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'os_version': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'package_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'postal_code': ('django.db.models.fields.IntegerField', [], {'max_length': '10', 'null': 'True'}),
            'priority': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'rating_scale': ('django.db.models.fields.IntegerField', [], {'max_length': '10', 'null': 'True'}),
            'reason': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'referrer_type': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'region': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'request_json': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'result': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'scope': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'screen_path': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'screen_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'sdk': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'sdk_version': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'send_next': (
            'django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'style': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'template_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'time_to_live': (
            'django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'variation_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'viewport_height': ('django.db.models.fields.IntegerField', [], {'max_length': '10', 'null': 'True'}),
            'viewport_width': ('django.db.models.fields.IntegerField', [], {'max_length': '10', 'null': 'True'})
        },
        'webhooks.journeyhook': {
            'Meta': {'object_name': 'JourneyHook'},
            'category': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'created_by': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'creation_time': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'done_at': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'done_by': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'event_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'event_time': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'journey_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'request_json': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'resource': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'tags': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'})
        }
    }

    complete_apps = ['webhooks']
