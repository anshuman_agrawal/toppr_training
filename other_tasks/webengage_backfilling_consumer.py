import logging
import os
import signal
import sys,requests


os.environ['DJANGO_SETTINGS_MODULE'] = 'haygot.settings'

ROOT_FOLDER = os.path.realpath(os.path.dirname(__file__))
ROOT_FOLDER = ROOT_FOLDER[:ROOT_FOLDER.rindex('/')]

if ROOT_FOLDER not in sys.path:
    sys.path.insert(1, ROOT_FOLDER + '/')

from django.conf import settings
from boto import sqs
from answers.models import AnswerSheet
from studyplan.models import UserBookmark
from users.models import UserAppInfo, UserProfile, UserOnboarded, ChallengeInvite, UserProfileData
from crm.models import Lead, CRMOfflinePincode, LeadCampaign, Communication, LeadEvent
from exams.models import Syllabus
from subscriptions.models import Subscription, Order
from django.utils import timezone
import utils
import datetime
from collections import defaultdict
from django.conf import settings
from django.utils import timezone
from django import db
import utils
# import webengage
from users.models import UserAppInfo, UserProfile, UserOnboarded, ChallengeInvite, UserProfileData
from crm.models import Lead, CRMOfflinePincode, LeadCampaign, Communication, LeadEvent
from exams.models import Syllabus
from subscriptions.models import Subscription, Order

log = logging.getLogger('www')
we_event_log = logging.getLogger('webengage_event')

url = 'https://api.webengage.com/v1/accounts/'
url += settings.WEBENGAGE_LICENSE_CODE
url += '/users'
webengage_session = requests.session()
headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer %s' % settings.WEBENGAGE_API_KEY,
}
def_tzone = timezone.get_default_timezone()

ACCESS_KEY = getattr(settings, 'AWS_SQS_ACCESS_KEY_ID', None)
SECRET_KEY = getattr(settings, 'AWS_SQS_SECRET_ACCESS_KEY', None)
REGION_NAME = getattr(settings, 'AWS_SQS_REGION_NAME', None)
ENVIRONMENT_PREFIX = getattr(settings, 'AWS_SQS_ENVIRONMENT_PREFIX', None)

kill = False


def signal_handler(sig, frame):
    global kill
    kill = True


signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)

def send_data(event):
    status_code = 0
    try:
        from copy import deepcopy
        payload=deepcopy(event)
        r=webengage_session.post(url, headers=headers, json=payload, timeout=30)
        status_code = r.status_code
        if status_code in (200, 201):
            resp = r.json()
            we_event_log.info('webengage backfilling: we_response for usqer %s is %s',event['userId'], resp)
        else:
            we_event_log.info('webengage backfilling: failed web_engage backfilling u:%s', event['userId'])
            we_event_log.error('webengage backfilling: %s %s error sending users to webengage', status_code,
                           'bad_request' if status_code == 400 else 'server')
    except requests.exceptions.Timeout:
        # timeout
        we_event_log.error('webengage backfilling: %s %s error sending users to webengage', status_code, 'timeout')
        we_event_log.error('webengage backfilling: failed web_engage backfilling u:%s', event['userId'])
        we_event_log.info('webengage backfilling: timeout error')
    except requests.exceptions.ConnectionError:
        # network error
        we_event_log.error('webengage backfilling: %s %s error sending users to webengage', status_code, 'connection')
        we_event_log.error('webengage backfilling: failed web_engage backfilling u:%s', event['userId'])
        we_event_log.info('webengage backfilling: connection error')
    except:
        # unknown error
        we_event_log.exception('webengage backfilling: %s %s error sending users to webengage', status_code, 'unknown')
        we_event_log.info('webengage backfilling: unknown error')
        we_event_log.error('webengage backfilling: failed web_engage backfilling u:%s', event['userId'])


def get_user_event(user):
    user_event={}
    up = user
    user_viewing_syllabus = up.get_viewing_syllabus()
    user_event.update({
        'id': up.id,
        'lead_id': up.lead_id,
        'signedup_on': up.signedup_on.astimezone(def_tzone).isoformat(),
        'name': up.get_name(),
        'email': up.email,
        'phone': up.get_e164_formatted_phone(),
        'type': 'paid' if up.is_paid_user() else 'unpaid',
        'trial_expires_on': up.get_trial_end_date().strftime('%Y-%m-%d'),
    })

    user_country = up.country
    if user_country:
        user_event['location_country'] = user_country.name
        user_event['location_country_iso_code'] = user_country.iso_code
    user_state, user_city = up.get_user_state_city()
    if user_state:
        user_event['location_state'] = user_state.name
    if user_city:
        user_event['location_city'] = user_city.name

    is_profile_complete = up.is_profile_complete()
    user_event['is_onboarded'] = is_profile_complete
    if is_profile_complete:
        try:
            user_event['onboarded_on'] = UserOnboarded.objects.get(user=up).last_updated_on.isoformat()
        except UserOnboarded.DoesNotExist:
            pass
    saleable_modules = utils.get_saleable_modules()

    user_subscriptions = Subscription.objects.select_related('module').filter(
        user_profile=up, module__in=saleable_modules).order_by('-id')
    for user_subscription in user_subscriptions:
        start_key = 'module_%s_starts_on' % user_subscription.module.name
        if user_event.get(start_key):
            continue
        user_event[start_key] = user_subscription.starts_from.isoformat()
        end_key = 'module_%s_expires_on' % user_subscription.module.name
        user_event[end_key] = user_subscription.ends_on.isoformat()

    if user_viewing_syllabus:
        syllabus = user_viewing_syllabus.syllabus
        if syllabus:
            user_event.update({
                'klass': user_viewing_syllabus.klass,
                'stream': syllabus.stream.slug if syllabus.stream else '',
                'board': syllabus.board.name if syllabus.board else '',
                'school': up.get_school_name(),
                'syllabus_id': syllabus.id,
                'syllabus_name': syllabus.name,
            })
            syllabus_exams = syllabus.get_exams()
            if syllabus_exams:
                user_event['exams'] = ';'.join([exam.name for exam in syllabus_exams])

    try:
        user_profile_data = UserProfileData.objects.get(user=up)
        loc_pincode = user_profile_data.location_pincode
        user_event['pincode'] = loc_pincode
    except:
        pass

    if up.lead:
        lead = up.lead
        if lead.pincode:
            co_pincodes = CRMOfflinePincode.objects.select_related('cluster').filter(pincode=lead.pincode)
            if co_pincodes:
                co_pincode = co_pincodes[0]
                if co_pincode.cluster:
                    crm_offline_cluster = co_pincode.cluster
                    user_event['cluster'] = crm_offline_cluster.name
                    if crm_offline_cluster.territory:
                        user_event['territory'] = crm_offline_cluster.territory.name
        if lead.current_experiment:
            user_event['lead_campaign_id'] = lead.current_experiment_id

        lcs = LeadCampaign.objects.filter(lead=lead)
        if lcs:
            lc = lcs[0]
            user_event['lead_status'] = lc.status
            user_event['assigned_to_agent_id'] = lc.assigned_to_id
            if lc.assigned_to:
                user_event['assigned_to_agent_name'] = lc.assigned_to.get_name()

        comms = Communication.objects.select_related('experiment').filter(lead=lead)
        if comms:
            com=comms[0]
            if com.experiment:
                user_event['lead_campaign_name'] = com.experiment.name

        # check if ordering is to be done in desc/asc order
        les = LeadEvent.objects.filter(lead=lead, event_type='demo').order_by('-scheduled_at')
        if les:
            lead_event = les[0]
            user_event['demo_scheduled_on'] = lead_event.scheduled_at.isoformat()

    orders = Order.objects.filter(user_profile=up).order_by('-expires_on')
    if orders:
        user_event['max_expiry_date'] = orders[0].expires_on.isoformat()

    signed_up_on = up.signedup_on
    if signed_up_on:
        delta = datetime.datetime.now().year - signed_up_on.year
        if up.klass:
            cla = up.klass
            cla = cla.split(' ')[0].strip('th').strip('+')
            try:
                cla = int(cla)
                if signed_up_on.month < 6:
                    if cla + delta <= 13:
                        user_event['current_class'] = cla + delta
                else:
                    if cla + delta <= 14:
                        user_event['current_class'] = cla + delta - 1
            except:
                pass

    return {
        'userId': str(up.id),
        'attributes': user_event,
        'email': up.email,
        'firstName': up.get_raw_name(),
        'phone': up.get_e164_formatted_phone(),
    }


def handle_message(msg):
    log.info(msg.get_body())
    start_id, end_id = msg.get_body().split(':')
    # log.info('%s %s',start_id,end_id)
    users = UserProfile.objects.filter(id__gte=start_id, id__lte=end_id)
    for user in users:
        we_event_log.info('webengage backfilling: u:%s is being sent', user.id)
        event = None
        try:
            event = get_user_event(user)
        except:
            we_event_log.error('webengage backfilling: u:%s error_we_user in getting user event', user.id)

        if (event):
            try:
                send_data(event)
                we_event_log.info('webengage backfilling: sending data for u:%s whose klass is %s', user.id, user.klass)
            except:
                we_event_log.error('webengage backfilling: u:%s error_we_user in sending data', user.id)
    return True


def main():
    conn = sqs.connect_to_region(REGION_NAME, aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_KEY)
    queue = conn.get_queue('%s__%s' % (ENVIRONMENT_PREFIX, 'webengage_backfilling'))
    if queue:
        while not kill:
            msgs = queue.get_messages()
            if (msgs):
                msg = msgs[0]
                if handle_message(msg):
                    queue.delete_message(msg)


if __name__ == "__main__":
    main()
