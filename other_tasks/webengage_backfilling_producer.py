import logging
import os
import sys,signal

os.environ['DJANGO_SETTINGS_MODULE'] = 'haygot.settings'

ROOT_FOLDER = os.path.realpath(os.path.dirname(__file__))
ROOT_FOLDER = ROOT_FOLDER[:ROOT_FOLDER.rindex('/')]

if ROOT_FOLDER not in sys.path:
    sys.path.insert(1, ROOT_FOLDER + '/')

from django.conf import settings
from boto import sqs
from boto.sqs.message import Message

log = logging.getLogger('www')
we_event_log = logging.getLogger('webengage_event')

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

ACCESS_KEY = getattr(settings, 'AWS_SQS_ACCESS_KEY_ID', None)
SECRET_KEY = getattr(settings, 'AWS_SQS_SECRET_ACCESS_KEY', None)
REGION_NAME = getattr(settings, 'AWS_SQS_REGION_NAME', None)
ENVIRONMENT_PREFIX = getattr(settings, 'AWS_SQS_ENVIRONMENT_PREFIX', None)
sqs_conn = sqs.connect_to_region(REGION_NAME, aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_KEY)
from users.models import UserAppInfo, UserProfile, UserOnboarded, ChallengeInvite, UserProfileData
from django.utils import timezone

def_tzone = timezone.get_default_timezone()




def send_to_queue(cur_start,cur_end,size):
    batch = str(cur_start) + ':' + str(cur_end)
    we_event_log.info('webengage backfilling: sending data from %s to %s', cur_start, cur_end)
    we_event_log.info('webengage backfilling: batch size is: %s', cur_end - cur_start + 1)
    try:
        body = batch
        qname = 'webengage_backfilling'
        we_event_log.info('webengage backfilling: routing qname %s' % qname)
        queue = sqs_conn.get_queue('%s__%s' % (ENVIRONMENT_PREFIX, qname))

        if not queue:
            queue = sqs_conn.create_queue('%s__%s' % (ENVIRONMENT_PREFIX, qname))

        queue.write(Message(body=str(body)))
        we_event_log.info('webengage backfilling: message to sqs %s sent is %s', qname, str(body))
    except:
        we_event_log.exception('webengage backfilling: routing Error queueing to SQS - %s' % body)
    cur_start += size

def main():
    if (len(sys.argv) < 4):
        we_event_log.error('webengage backfilling: arguments error')
        sys.exit(0)
    try:
        # start = 3351459
        size = int(sys.argv[3])
        # last=4318336
        start = int(sys.argv[1])
        last = int(sys.argv[2])
        cur_start = start
    except:
        we_event_log.error('webengage backfilling: arguments error')
        sys.exit(0)
    while True:
        cur_end = cur_start+size-1

        if cur_end <= last:
            send_to_queue(cur_start,cur_end,size)
        elif cur_end-last < size:
            send_to_queue(cur_start,last,size)
        else:
            break
        cur_start+=size



if __name__ == "__main__":
    main()
